<?php include("inc/header.php");?>
<div class="content">
<div class="lc">
<h2>All Products</h2>

<?php
	if(!isset($_GET['id'])){
	include('inc/paginator.class.php');
	if (isset($_GET['subcat']))
		$query = "SELECT COUNT(*) FROM products WHERE subcat=".$_GET['subcat']." ORDER BY sorting DESC";
	else
		$query = "SELECT COUNT(*) FROM products ORDER BY sorting DESC";
	
	$result = $conn->query($query);
	$num_rows = mysqli_fetch_row($result);

	$pages = new Paginator;
	$pages->items_total = $num_rows[0];
	$pages->mid_range = 3; // Number of pages to display. Must be odd and > 3
	$pages->paginate();

	echo '<div class="pagination round">';
	//echo $pages->display_pages();
	echo $pages->display_jump_menu().$pages->display_items_per_page();
	echo '<div class="clear"></div></div>';
	}
	if(isset($_GET['subcat']))
		$query = "SELECT * FROM products WHERE subcat=".$_GET['subcat']." ORDER BY sorting DESC $pages->limit";
	else if(isset($_GET['id']))
		$query = "SELECT * FROM products WHERE id=".$_GET['id'];
	else
		$query = "SELECT * FROM products ORDER BY sorting DESC $pages->limit";
	
	$query = $conn->query($query);
	if($query->num_rows > 0){
		while($result = $query->fetch_assoc()){
			$dis = $result['sprice'];
			if($result['distype']==2) 
				$dis = $result['sprice']-$result['discount'];
			else if($result['discount']) 
				$dis = $result['sprice']-($result['sprice']*($result['discount']/100));
?>



<div class="product round">
<img src="update/upload/products/<?php if(!empty($result['image'])){echo $result['image'];}else{echo "noimage.png";}?>" alt="<?=$result['title'];?>" height="140" width="150">
	<div class="preview">
	<h3><?=$result['title'];?> <span class="date">Added on <?=$result['lastupdate'];?></span>
	<span class="np">৳<?=$dis;?></span>
	<?php
	if($result['sprice']!=$dis){ ?>
	<span class="price">৳<?=$result['sprice'];?></span>
	<?php } ?> </h3>
	<p><?=(!isset($_GET['id']))?limit_text($result['content'],25):$result['content'];?></p>
	</div>
<div class="clear"> </div>
<a href="mycart.php?id=<?=$result['id'];?>">Add to cart</a>
<?php
if(!isset($_GET['id'])){
?>
<a href="products.php?id=<?=$result['id'];?>">Details</a>	
<?php
}
else {
?>
<a href="<?=$_SERVER['HTTP_REFERER'];?>">Go Back</a>
<?php } ?>
<div class="clear"></div>
</div>	

<?php
		}
	}
?>
<div class="clear"></div>
<?php
if(!isset($_GET['id'])){
?>
<div class="pagination round">
	<?php
	echo "<p class=\"paginate\">Page: $pages->current_page of $pages->num_pages</p>\n";
	?>
	<div class="numbs">
	<?php
	echo $pages->display_pages();
	//echo "<p class=\"paginate\">SELECT * FROM table $pages->limit (retrieve records $pages->low-$pages->high from table - $pages->items_total item total / $pages->items_per_page items per page)";
	?>
	</div>
	<div class="clear"></div>
</div>
<?php } ?>
</div> <!-- End of lc -->
<?php include("inc/footer.php");?>