<?php include("inc/header.php");?>
<div class="content">
<div class="lc">
<h2>Featured Products</h2>

<?php
	include('inc/paginator.class.php');
	$query = "SELECT COUNT(*) FROM products WHERE feature=1 ORDER BY sorting DESC";
	
	$result = $conn->query($query);
	$num_rows = mysqli_fetch_row($result);

	$pages = new Paginator;
	$pages->items_total = $num_rows[0];
	$pages->mid_range = 3; // Number of pages to display. Must be odd and > 3
	$pages->paginate();

	echo '<div class="pagination round">';
	//echo $pages->display_pages();
	echo $pages->display_jump_menu().$pages->display_items_per_page();
	echo '<div class="clear"></div></div>';
?>

<?php
	$query = "SELECT * FROM products WHERE feature=1 ORDER BY sorting DESC $pages->limit";
	$query = $conn->query($query);
	if($query->num_rows>0){
		while($result = $query->fetch_assoc()){
			$dis = $result['sprice'];
			if($result['distype']==2) 
				$dis = $result['sprice']-$result['discount'];
			else if($result['discount']) 
				$dis = $result['sprice']-($result['sprice']*($result['discount']/100));
			
	?>
	<div class="img">
	<img src="update/upload/products/<?php if(!empty($result['image'])){echo $result['image'];}else{echo "noimage.png";}?>" alt="<?=$result['title'];?>" width="300" height="200">
	<div class="desc">
	<h3><?=$result['title'];?> </h3>
	<?php
	if($result['sprice']!=$dis){ ?>
	<span class="del">৳<?=$result['sprice'];?></span>
	<?php } ?>
	<span class="ins">৳<?=$dis;?></span>	
	<a href="mycart.php?id=<?=$result['id'];?>">Add to cart</a>
	</div>
	</div>
<?php
		}
	}
?>
<div class="clear"></div>
<?php
if(!isset($_GET['id'])){
?>
<div class="pagination round">
	<?php
	echo "<p class=\"paginate\">Page: $pages->current_page of $pages->num_pages</p>\n";
	?>
	<div class="numbs">
	<?php
	echo $pages->display_pages();
	//echo "<p class=\"paginate\">SELECT * FROM table $pages->limit (retrieve records $pages->low-$pages->high from table - $pages->items_total item total / $pages->items_per_page items per page)";
	?>
	</div>
	<div class="clear"></div>
</div>
<?php } ?>
</div> <!-- End of lc -->

<?php include("inc/footer.php");?>