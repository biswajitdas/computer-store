<?php require_once("inc/session.php");?>
<?php require_once("inc/connect.php");?>
<?php require_once("inc/functions.php");?>
<?php
	if(logged_in_customer()){
		redirect_to("user.php");
	}
	include_once("inc/form_functions.php");
	
	//START PROCESSING
	
	if(isset($_POST['submit'])){  //IF FORM HAS BEEN SUBMITTED.
	$errors = array();
	
	//FORM VALIDATION
	$required_fields = array('username', 'password');
	$errors = array_merge($errors, check_required_fields($required_fields, $_POST));
	
	$username = trim(mysqli_real_escape_string($conn,$_POST['username']));
	$password = trim(mysqli_real_escape_string($conn,$_POST['password']));
	
	if(empty($errors)){
		$query = "SELECT id, username ";
		$query .= "FROM user ";
		$query .= "WHERE username = '{$username}' ";
		$query .= "AND password = '{$password}' ";
		$query .= "AND type = '1' ";
		$query .= "LIMIT 1";
		
		$result_set = $conn->query($query);
		confirm_query($result_set);
		
		if($result_set->num_rows == 1)
		{
		// username/password authenticated
		// and only 1 match
		$found_user = $result_set->fetch_assoc();
		$_SESSION['c_id'] = $found_user['id'];
		$_SESSION['cname'] = $found_user['username'];
		
		redirect_to("user.php");
		}else {
		$errors = "Wrong Username / Password !!";
		}
	
		}else{
		$message ="".count($errors)." Error/s in the form.";
		}
	
	}else {
		if(isset($_GET['logout']) && $_GET['logout'] == 1){
		$message = "You are now logged out.";
		}
		$username = "";
		$password = "";
	}

	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<title>Login Page</title>
	
	<style type="text/css">
	
*{ margin: 0; padding: 0; }
body{ font-family: Georgia, serif; background: url(images/login-page.jpg) top center no-repeat #c4c4c4; color: #3a3a3a;  }

.clear{ clear: both; }

form{ width: 406px; margin: 170px auto 0; }
form p{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FF3300;
	display: block;
	text-align: center;
	padding: 2px 0;
	}
legend{ display: none; }

fieldset{ border: 0; }

label{ width: 115px; text-align: right; float: left; margin: 0 10px 0 0; padding: 9px 0 0 0; font-size: 16px; }

input{ width: 220px; display: block; padding: 4px; margin: 0 0 10px 0; font-size: 18px;
					  color: #3a3a3a; font-family: Georgia, serif;}
input[type=checkbox]{ width: 20px; margin: 0; display: inline-block; }
					  
.button{ background: url(images/button-bg.png) repeat-x top center; border: 1px solid #999;
					  -moz-border-radius: 5px; padding: 5px; color: black; font-weight: bold;
					  -webkit-border-radius: 5px; font-size: 13px;  width: 70px; }
.button:hover{ background: white; color: black; }
	</style>
</head>

<body>

	<form action="signin.php" method="POST" id="login-form">   
		<fieldset>
		
			<legend>Log in</legend>
			 <p>
			<?php
			if (!empty($errors)){
			echo "<p> $errors </p>";
			}   	
			?>

			<?php
			if (!empty($message)){
			echo "<p> $message </p>";
			}
			?>
		    </p>
			<label for="login-username">Username</label>
			<input type="text" name="username" id="login-username" class="round" autofocus="autofocus" />
			<div class="clear"></div>
			
			 <label for="login-password">Password</label>
			<input type="password" name="password" id="login-password" class="round" />
			<div class="clear"></div>
			
			<br />
			
			<input type="submit" style="margin: -20px 0 0 287px;" class="button" name="submit" value="Log in"/>	
		</fieldset>
	</form>
	
</body>

</html>