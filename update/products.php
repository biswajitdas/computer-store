<?php include("inc/header.php");?>	
<?php
// delete an image
if (isset($_GET['del']))
	{
	$basepath='upload/products/';
	$id=$_GET['del'];
		$db_query="SELECT * FROM products WHERE id=$id";
		$db_result=$conn->query($db_query);
		$del_image_name=$db_result->fetch_assoc();
		
		$old_image=$del_image_name['image'];
		
		$del_query="DELETE FROM products WHERE id=$id";
		$conn->query($del_query);
		
		if(mysqli_affected_rows($conn))
		{
			@unlink($basepath.$old_image);
		}
	}
?>
			<div class="rc round">
			<h2>Add Product</h2>
						
			
			<table id="list" class="info_table" width="730px" cellpadding="0" cellspacing="0">
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				
				<td>
				<form>				
				<select name="cat" id="cat" onChange="getProducts(0);">
					<option value="">Select Category</option>
					<?php
					$query = "SELECT * FROM category ORDER BY sorting DESC";
					$query = $conn->query($query);
					if($query->num_rows>0){
						while($row = $query->fetch_assoc()){
					?>
					<option <?=($_GET['cat']==$row['id'])?'selected':'';?> value="<?=$row['id'];?>"><?=$row['title'];?></option>
					<?php
						}
					}
					?>
					
				</select>				
				</form>
				</td>
				<td>
				<form>
				<select name="subcat" id="subcat" onChange="getProducts(1);">
					<option value="">Select Sub Category</option>
					<?php
					if(isset($_GET['cat']))
						$query = "SELECT * FROM subcat WHERE parent=".$_GET['cat']." ORDER BY sorting DESC";
					else
						$query = "SELECT * FROM subcat ORDER BY sorting DESC";
					$query = $conn->query($query);
					if($query->num_rows>0){
						while($row = $query->fetch_assoc()){
					?>
					<option <?=($_GET['subcat']==$row['id'])?'selected':'';?> value="<?=$row['id'];?>"><?=$row['title'];?></option>
					<?php
						}
					}
					?>
				</select>
				</form>
				</td>
			</tr>
			<tr>
			<th width="10%">Serial</th>
			<th width="18%">Title</th>
			<th width="10%">Unit Price</th>
			<th width="10%">Sale Price</th>			
			<th width="10%">Featured</th>
			<th width="10%">Qantity</th>			
			<th width="14%">Last Update</th>
			<th width="18%">ACTIONS</th>
			</tr>

			<?php
			if(isset($_GET['subcat']))
				$query = "SELECT * FROM products WHERE subcat = ".$_GET['subcat']." ORDER BY sorting DESC";
			else if(isset($_GET['cat']))
				$query = "SELECT * FROM products WHERE category = ".$_GET['cat']." ORDER BY sorting DESC";
			else
				$query = "SELECT * FROM products ORDER BY sorting DESC";
			$query = $conn->query($query);
			if($query->num_rows > 0){
			$i=1;
				while($row = $query->fetch_assoc()){
			?>

			<tr>
				<td><?=$i++;?></td>
				<td><?=$row['title'];?></td>
				<td><?=$row['uprice'];?>TK</td>
				<td><?=$row['sprice'];?>TK</td>
				<td><?=($row['feature'])?'YES':'NO';?></td>
				<td><?=$row['quantity'];?></td>
				<td><?=$row['lastupdate'];?></td>
				<td>
				<ul class="action_link">
				<li> <a href="addproducts.php?edit=<?=$row['id'];?>" class="edit" title="Edit">A</a> </li>
				<li> <a href="products.php?del=<?=$row['id'];?>" onclick="return confirm('Are you sure ?');" class="del" title="Delete">A</a> </li>
				<li> <a href="sorting.php?db=2&amp;id=<?=$row['id'];?>&amp;move=up" class="up" title="Up">A</a> </li>
				<li> <a href="sorting.php?db=2&amp;id=<?=$row['id'];?>&amp;move=down" class="down" title="Down">A</a> </li>
				</ul>
				</td>
			</tr>
			<?php
				}
			}
			?>
			</table>
			</div>
			<div class="clear"></div>
		</div>
	</div>
		<script type="text/javascript">
		function getProducts(x){
			var subcat,cat;
			if(x){
				subcat = document.getElementById('subcat').value;
				cat = document.getElementById('cat').value;
				if(cat)
				window.location = 'products.php?cat='+cat+'&subcat='+subcat;
				else if(subcat)
					window.location = 'products.php?subcat='+subcat;
				else
					window.location = 'products.php';
			}
			else{
				cat = document.getElementById('cat').value;
				if(cat)
					window.location = 'products.php?cat='+cat;
				else
					window.location = 'products.php';
			}
				
		}
		</script>
</body>
</html>