<?php require_once("inc/session.php"); ?>
<?php require_once("inc/connect.php"); ?>
<?php require_once("inc/functions.php"); ?>
<?php confirm_logged_in_customer(); ?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Computers Store Admin Panel</title>
	<link rel="stylesheet" type="text/css" href="data/style.css" />
	<link rel="stylesheet" href="redactor/css/redactor.css" />
    <script type="text/javascript" src="data/jquery-1.7.min.js"></script>	

	<script src="redactor/redactor.js"></script>
	
	<script type="text/javascript"> 
	$(document).ready(
		function()
		{
			$('#redactor').redactor({ focus: true });
		}
	);
	
	$(document).ready(function(){		
		$("#msg").animate({opacity:'hide'},5000);
	});
		
	</script>
		
</head>
<body>
	<div class="wrapper round">
		<div class="header roundbottom">
		<h2>Computers Store</h2>
			<div class="topnav">
			<ul>
				<li><a href="http://biswajitdas.me/projects/cstore" target="_blank">Back to website</a></li>
				<li><a href="logout.php">Log Out</a></li>
			</ul>
			</div>
		</div>
		<div class="content round boxshadow">
			<div class="lc">							
				<div class="sidenav round">
				<h2>Manage Account</h2>
				<ul>
					<li><a href="user.php">My Orders</a></li>
					<li><a href="changepass.php">Change Password</a></li>
				</ul>
				</div>
			</div>	

			<div class="rc round">
			<h2>Sub Catagories</h2>
			
			<table class="info_table" width="730px" cellpadding="0" cellspacing="0">
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>
				<form>
				<select id="id" onChange="check();">					
						<option value=""> Select </option>
						<option <?=($_GET['status'])?'selected':'';?> value="1"> Accepted </option>
						<option <?=($_GET['status']==0)?'selected':'';?> value="0"> Pending </option>
				</select>
				</form>
				</td>
			</tr>
<tr>
<th width="10%">Serial</th>
<th width="25%">Title</th>
<th width="20%">Quantity</th>
<th width="20%">Price</th>
<th width="10%">Accepted</th>
<th width="15%">Action</th>
</tr>

<?php

if(isset($_GET['id'])){
	$query = "SELECT * FROM products WHERE id=".$_GET['id'];
	$query = $conn->query($query);
	$result = $query->fetch_assoc();
	
	$dis = $result['sprice'];
	if($result['distype']==2) 
		$dis = $result['sprice']-$result['discount'];
	else if($result['discount']) 
		$dis = $result['sprice']-($result['sprice']*($result['discount']/100));	
	
	if($_GET['move']=='up'){
		$query = "SELECT * FROM orders WHERE id=".$_GET['oid'];
		$query = $conn->query($query);
		$res = $query->fetch_assoc();
		$qty = $res['quantity']+1;
		$price = $res['price']+$dis;
		$query = "UPDATE orders SET quantity='".$qty."', price = '".$price."' WHERE id=".$_GET['oid'];
		$conn->query($query);
	}
	
	else if($_GET['move']=='down'){
		$query = "SELECT * FROM orders WHERE id=".$_GET['oid'];
		$query = $conn->query($query);
		$res = $query->fetch_assoc();
		$qty = $res['quantity']-1;
		$price = $res['price']-$dis;
		$query = "UPDATE orders SET quantity='".$qty."', price = '".$price."' WHERE id=".$_GET['oid'];
		$conn->query($query);
	}
	else{
	$query = "INSERT INTO `orders` (pid,quantity,price,cid,date,accepted) 
			VALUES (".$_GET['id'].",'1',".$dis.",".$_SESSION['c_id'].",'".date("Y-m-d")."','0')";
	}	
	$conn->query($query);
}

if(isset($_GET['del'])){
	$query = "DELETE FROM orders WHERE id = '".$_GET['del']."'";
	$conn->query($query);
}

if(isset($_GET['status']))	
	$query = "SELECT * FROM orders WHERE cid = ".$_SESSION['c_id']." AND accepted=".$_GET['status'];
else
	$query = "SELECT * FROM orders WHERE cid = ".$_SESSION['c_id'];
	
	

	$query = $conn->query($query);
	$i=1;
	$total=0;
	if($query->num_rows > 0){
		while($res = $query->fetch_assoc()){
			$q = "SELECT * FROM products WHERE id=".$res['pid'];
			$q = $conn->query($q);
			$q = $q->fetch_assoc();
			$total+=$res['price'];
?>
<tr>
<td><?=$i++;?></td>
<td><?=$q['title'];?></td>
<td><?=$res['quantity'];?></td>
<td>৳<?=$res['price'];?></td>
<td><?=($res['accepted'])?'YES':'NO';?></td>
<td>
<ul class="action_link">
<li> <a href="user.php?id=<?=$res['pid'];?>&amp;oid=<?=$res['id'];?>&amp;move=up" class="up" title="Up">A</a> </li>
<li> <a href="user.php?id=<?=$res['pid'];?>&amp;oid=<?=$res['id'];?>&amp;move=down" class="down" title="Down">A</a> </li>
<li> <a href="user.php?del=<?=$res['id'];?>" class="del" onclick="return confirm('Are you sure ?');" title="Delete">A</a> </li>
</ul>
</td>
</tr>
<?php			
		}
	}
?>



<tr>
<td></td>
<td></td>
<td>Total Price :</td>
<td>৳<?=$total;?></td>
<td></td>
</tr>

</table>			

			
			</div>
			<div class="clear"></div>
		</div>
	</div>
<script type="text/javascript">
function check(){
	var id = document.getElementById('id').value;
	window.location = 'user.php?status='+id;
}
</script>
</body>
</html>