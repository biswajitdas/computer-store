<?php include("inc/header.php");?>	
<?php
    // make image directory
    $basepath='upload/';
    @mkdir($basepath,0777);
    @chmod($basepath,0777);

    $basepath="upload/products/";
    @mkdir($basepath);
    @mkdir($basepath,0777);
    @chmod($basepath,0777);

    $errors='';

//settings
$max_allowed_file_size = 400; // size in KB
$allowed_extentions = array("jpg", "jpeg","png");

    // insert new data
    if($_POST['save']=='Submit')
    {
                
        $query = "INSERT INTO `products` (title, content, uprice, sprice, feature, discount, distype, quantity, lastupdate, sorting, subcat, category) 
        VALUES ('".jp_encode($_POST['title'])."', 
                '".jp_encode($_POST['content'])."', 
                '".jp_encode($_POST['uprice'])."', 
                '".jp_encode($_POST['sprice'])."', 
                '".jp_encode($_POST['mf'])."', 
                '".jp_encode($_POST['discount'])."', 
                '".jp_encode($_POST['distype'])."', 
                '".jp_encode($_POST['qty'])."', 
				'".date("Y-m-d")."',
                '".time()."',
				'".$_REQUEST['subcat']."',				
                '".$_POST['category']."'
				)";
                
        $conn->query($query);     
        
    
        
        $whatsnewid=$conn->insert_id;  
        //Get the uploaded file information
        $name_of_the_uploaded_file= basename($_FILES['uploaded_image']['name']);
    
        //Get the file extention
        $type_of_uploaded_file = substr($name_of_the_uploaded_file, strrpos($name_of_the_uploaded_file, '.') +1 );
        $size_of_uploaded_file = $_FILES['uploaded_image']['size']/1024;
        
        //Do validation
                
        if($size_of_uploaded_file > $max_allowed_file_size)
        {
        $errors .= "Size of the image should be less than $max_allowed_file_size <br/>";
        }
        
        //Validate file extention
        $allowed_ext=false;
        for ($i=0; $i<sizeof($allowed_extentions); $i++)
        {
            if(strcasecmp($allowed_extentions[$i],$type_of_uploaded_file)== 0)
            {
                $allowed_ext=true;
            }
        }
        
        if (!$allowed_ext)
        {
        $errors .= "Only the following image types are supported: ".implode(',',$allowed_extentions)."<br/>";
        }
        // rename the file
        $result=explode(".", $name_of_the_uploaded_file);
        $demoname="pro_".time().".".strtolower(array_pop($result));
        
        
        if (empty($errors))
        {
            $query="UPDATE products SET image='".$demoname."' WHERE id ='".$whatsnewid."' ";
            $conn->query($query);
            
        
            //copy the temp. uploaded file to upload folder
            $path_of_uploaded_file = $basepath . $demoname;
            $tmp_path = $_FILES['uploaded_image']['tmp_name'];
            
            if (is_uploaded_file($tmp_path))
            {
                if (!copy($tmp_path,$path_of_uploaded_file))
                {
                    $errors .='Error while copying the uploaded image.<br/>';
                }
            }
        }       
    }
	
	//update existing data


    if(isset($_POST['update']))
    {
        $query = "UPDATE products SET				
				title='".jp_encode($_POST['title'])."', 
                content='".jp_encode($_POST['content'])."', 
                uprice='".jp_encode($_POST['uprice'])."', 
                sprice='".jp_encode($_POST['sprice'])."',  
                feature='".jp_encode($_POST['mf'])."', 
                discount='".jp_encode($_POST['discount'])."',
				distype='".jp_encode($_POST['distype'])."',
				subcat='".jp_encode($_POST['subcat'])."',
				category='".jp_encode($_POST['category'])."',
				lastupdate='".date("Y-m-d")."'						
        WHERE id='".$_POST['h_id']."' ";

		$conn->query($query);        
        
        if(!empty($_FILES["uploaded_image"]["name"])){

        @unlink($basepath.$_POST['old_img']);
        $query_string="UPDATE products SET image='' WHERE id ='".$_POST['h_id']."' ";
        $conn->query($query_string);
    
        //Get the uploaded file information
        $name_of_the_uploaded_file= basename($_FILES['uploaded_image']['name']);
    
        //Get the file extention
        $type_of_uploaded_file = substr($name_of_the_uploaded_file, strrpos($name_of_the_uploaded_file, '.') +1 );
        $size_of_uploaded_file = $_FILES['uploaded_image']['size']/1024;
        
        //Do validation
                
        if($size_of_uploaded_file > $max_allowed_file_size)
        {
        $errors .= "Size of the image should be less than $max_allowed_file_size <br/>";
        }
        
        //Validate file extention
        $allowed_ext=false;
        for ($i=0; $i<sizeof($allowed_extentions); $i++)
        {
            if(strcasecmp($allowed_extentions[$i],$type_of_uploaded_file)== 0)
            {
                $allowed_ext=true;
            }
        }
        
        if (!$allowed_ext)
        {
        $errors .= "Only the following image types are supported: ".implode(',',$allowed_extentions)."<br/>";
        }
        // rename the file
        
        $result=explode(".", $name_of_the_uploaded_file);
        $demoname="pro_".time().".".strtolower(array_pop($result));
        
        
        if (empty($errors))
        {
            $query="UPDATE products SET image='".$demoname."' WHERE id ='".$_POST['h_id']."' ";
            $conn->query($query);            
        
            //copy the temp. uploaded file to upload folder
            $path_of_uploaded_file = $basepath . $demoname;
            $tmp_path = $_FILES['uploaded_image']['tmp_name'];
            
            if (is_uploaded_file($tmp_path))
            {
                if (!copy($tmp_path,$path_of_uploaded_file))
                {
                    $errors .='Error while copying the uploaded image.<br/>';
                }
            }
        }
    }   
    }
	
// delete an image
if (isset($_GET['del']))
	{
	$basepath='upload/products/';
	$id=$_GET['del'];
		$db_query="SELECT * FROM products WHERE id=$id";
		$db_result=$conn->query($db_query);
		$del_image_name=$db_result->fetch_assoc();
		
		$old_image=$del_image_name['image'];
		
		$del_query="DELETE FROM products WHERE id=$id";
		$conn->query($del_query);
		
		if(mysqli_affected_rows($conn))
		{
			@unlink($basepath.$old_image);
		}
	}
	
	    // retrive data for edit
    if(isset($_GET['edit'])){
    $query="SELECT * FROM products WHERE id = ".$_GET['edit'];
    $query_result=$conn->query($query);
    $data=$query_result->fetch_assoc();
    }
?>
			<div class="rc round">
			<h2>Add Product</h2>
			<form class="form_area" action="addproducts.php" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="h_id" id="h_id" value="<?=$data['id'];?>"/>
				<input type="hidden" name="old_img" value="<?=$data['image'];?>">

				<table cellpadding="0" cellspacing="0" width="730px" class="form_table">
					<tr>
						<td colspan="2">
							<?php
								if (!empty($errors)) {
									echo "<span style=\"color:red; display:block; background-color:yellow; text-align:center\"> ** {$errors} </span>";
								}else{
									echo "&nbsp;";
								}
							?>
						   

						</td>
					</tr>
					<tr>
						<td>Select Catagory</td>
						<td>
						<select name="category" id="category" onchange="getSubCat()">
							<option value="">Select Catagory</option>
						 <?php
						 $query = "SELECT * FROM category ORDER BY sorting DESC";
						 $result = $conn->query($query);
						 if($result->num_rows>0){
							 while($res = $result->fetch_assoc()){
						?>
						<option <?=(($_GET['category']==$res['id'])||($data['category']==$res['id']))?'selected':''?> value="<?=$res['id'];?>"><?=$res['title'];?></option>
						<?php
							 }
						 }
						?>
						</select>
						</td>
					</tr>					
					
					<tr>
						<td>Select Sub Catagory</td>
						<td>
						<select name="subcat">
							<option value="">Select Subcat</option>
						<?php
						if(isset($_GET['category']))
							$query = "SELECT * FROM subcat WHERE category=".$_GET['category']." ORDER BY sorting DESC";
						else if(isset($_GET['edit']))
							$query = "SELECT * FROM subcat WHERE category=".$data['category']." ORDER BY sorting DESC";
						else
							$query = "SELECT * FROM subcat ORDER BY sorting DESC";
						 $result = $conn->query($query);
						 if($result->num_rows>0){
							 while($res = $result->fetch_assoc()){
						?>
						<option <?=($data['subcat']==$res['id'])?'selected':'';?> value="<?=$res['id'];?>"><?=$res['title'];?></option>
						<?php
							 }
						 }
						?>
						</select>
						</td>
					</tr>

					<tr>
						<td width="104">Product Title</td>
						<td width="624">
							<input type="text" name="title" value="<?=$data['title'];?>"/>
						</td>
					</tr>

					<tr>
						<td>Unit Price</td>
						<td>
							<input type="text" name="uprice" value="<?=$data['uprice'];?>" />
						</td>
					</tr>
					
					<tr>
						<td>Sale Price</td>
						<td>
							<input type="text" name="sprice" value="<?=$data['sprice'];?>" />
						</td>
					</tr>
					
					<tr>
						<td>Quantity</td>
						<td>
							<input type="number" name="qty" value="<?=$data['quantity'];?>" min="1"/>
						</td>
					</tr>
					
					<tr>
						<td>Discount</td>
						<td>
							<input type="number" name="discount" value="<?=$data['discount'];?>" min="0"/>
						</td>
					</tr>
					
					<tr>
						<td>Discount type</td>
						<td>
						<select name="distype">
							<option <?=($data['distype']==1)?'selected':'';?> value="1">Parcent</option>
							<option <?=($data['distype']==2)?'selected':'';?> value="2">Dollar</option>
						</select>
						</td>
					</tr>
					
					<tr>
						<td>Upload image<br /> 160px * 160px </td>
						<td>
							<input type="file" name="uploaded_image"/>
						</td>
					</tr>
					<tr>
						<td>
						<label for="mf">Make Feature</label> 
						</td>
						<td>
						<input type="checkbox" <?=($data['feature']==1)?'checked=checked':'';?> name="mf" id="mf" value="1"/>
						</td>
					</tr>
					
					<tr>
					<td >Product's Details</td>
					<td></td>
					</tr>

				<tr>
					<td colspan="2">
					<textarea name="content" id="redactor"><?=$data['content'];?></textarea>
					</td>
				</tr>


					<tr>
						<td>&nbsp;</td>
						<td>
							<?php
								if(isset($_GET['edit']))
								{										
									echo '<input class="button" name="update" value="Update" type="submit"/>';
								}
								else
								{  
									echo '<input class="button" name="save" value="Submit" type="submit"/>';
								}
							?>							
							<input class="button" name="reset" value="Reset" type="Reset"/>
						</td>
						
					</tr>

				</table>
			</form>
			
			
			<table id="list" class="info_table" width="730px" cellpadding="0" cellspacing="0">
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				
				<td>
				<form>				
				<select name="cat" id="cat" onChange="getProducts(0);">
					<option value="">Select Category</option>
					<?php
					$query = "SELECT * FROM category ORDER BY sorting DESC";
					$query = $conn->query($query);
					if($query->num_rows>0){
						while($row = $query->fetch_assoc()){
					?>
					<option <?=(($_GET['cat']==$row['id'])||($_POST['category']==$row['id']))?'selected':'';?> value="<?=$row['id'];?>"><?=$row['title'];?></option>
					<?php
						}
					}
					?>
					
				</select>				
				</form>
				</td>
				<td>
				<form>
				<select name="subcat" id="subcat" onChange="getProducts(1);">
					<option value="">Select Sub Category</option>
					<?php
					if(isset($_GET['cat']))
						$query = "SELECT * FROM subcat WHERE category=".$_GET['cat']." ORDER BY sorting DESC";
					else if(isset($_POST['category']))
						$query = "SELECT * FROM subcat WHERE category=".$_POST['category']." ORDER BY sorting DESC";
					else
						$query = "SELECT * FROM subcat ORDER BY sorting DESC";
					$query = $conn->query($query);
					if($query->num_rows>0){
						while($row = $query->fetch_assoc()){
					?>
					<option <?=(($_GET['subcat']==$row['id'])||($_POST['subcat']==$row['id']))?'selected':'';?> value="<?=$row['id'];?>"><?=$row['title'];?></option>
					<?php
						}
					}
					?>
				</select>
				</form>
				</td>
			</tr>
			<tr>
			<th width="10%">Serial</th>
			<th width="18%">Title</th>
			<th width="10%">Unit Price</th>
			<th width="10%">Sale Price</th>			
			<th width="10%">Featured</th>
			<th width="10%">Qantity</th>			
			<th width="14%">Last Update</th>
			<th width="18%">ACTIONS</th>
			</tr>

			<?php
			if(isset($_GET['cat']))
				$query = "SELECT * FROM products WHERE category = ".$_GET['cat']." ORDER BY sorting DESC";
			else if(isset($_GET['subcat']))
				$query = "SELECT * FROM products WHERE subcat = ".$_GET['subcat']." ORDER BY sorting DESC";
			else if(isset($_POST['subcat']))
				$query = "SELECT * FROM products WHERE subcat = ".$_POST['subcat']." ORDER BY sorting DESC";
			else
				$query = "SELECT * FROM products ORDER BY sorting DESC";
			$query = $conn->query($query);
			if($query->num_rows > 0){
			$i=1;
				while($row = $query->fetch_assoc()){
			?>

			<tr>
				<td><?=$i++;?></td>
				<td><?=$row['title'];?></td>
				<td><?=$row['uprice'];?>TK</td>
				<td><?=$row['sprice'];?>TK</td>
				<td><?=($row['feature'])?'YES':'NO';?></td>
				<td><?=$row['quantity'];?></td>
				<td><?=$row['lastupdate'];?></td>
				<td>
				<ul class="action_link">
				<li> <a href="addproducts.php?edit=<?=$row['id'];?>" class="edit" title="Edit">A</a> </li>
				<li> <a href="addproducts.php?del=<?=$row['id'];?>" onclick="return confirm('Are you sure ?');" class="del" title="Delete">A</a> </li>
				<li> <a href="sorting.php?db=2&amp;id=<?=$row['id'];?>&amp;move=up" class="up" title="Up">A</a> </li>
				<li> <a href="sorting.php?db=2&amp;id=<?=$row['id'];?>&amp;move=down" class="down" title="Down">A</a> </li>
				</ul>
				</td>
			</tr>
			<?php
				}
			}
			?>
			</table>
			</div>
			<div class="clear"></div>
		</div>
	</div>
		<script type="text/javascript">
		function getSubCat()
		{
			var catid = document.getElementById('category').value;
			var hid = document.getElementById('h_id').value;
			if(catid&&hid)
			window.location='addproducts.php?category='+catid+'&edit='+hid;
			else
				window.location = 'addproducts.php?category='+catid;
		}
		
		function getProducts(x){
			var subcat,cat;
			if(x){
				subcat = document.getElementById('subcat').value;
				cat = document.getElementById('cat').value;
				if(cat)
				window.location = 'addproducts.php?cat='+cat+'&subcat='+subcat;
				else if(subcat)
					window.location = 'addproducts.php?subcat='+subcat;
				else
					window.location = 'addproducts.php';
			}
			else{
				cat = document.getElementById('cat').value;
				if(cat)
					window.location = 'addproducts.php?cat='+cat;
				else
					window.location = 'addproducts.php';
			}
				
		}
		</script>
</body>
</html>