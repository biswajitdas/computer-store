<?php require_once("inc/session.php"); ?>
<?php require_once("inc/connect.php"); ?>
<?php require_once("inc/functions.php"); ?>
<?php confirm_logged_in_customer(); ?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Computers Store Admin Panel</title>
	<link rel="stylesheet" type="text/css" href="data/style.css" />
	<link rel="stylesheet" href="redactor/css/redactor.css" />
    <script type="text/javascript" src="data/jquery-1.7.min.js"></script>	

	<script src="redactor/redactor.js"></script>
	
	<script type="text/javascript"> 
	$(document).ready(
		function()
		{
			$('#redactor').redactor({ focus: true });
		}
	);
	
	$(document).ready(function(){		
		$("#msg").animate({opacity:'hide'},5000);
	});
		
	</script>
		
</head>
<body>
	<div class="wrapper round">
		<div class="header roundbottom">
		<h2>Computers Store</h2>
			<div class="topnav">
			<ul>
				<li><a href="http://biswajitdas.me/projects/cstore" target="_blank">Back to website</a></li>
				<li><a href="logout.php">Log Out</a></li>
			</ul>
			</div>
		</div>
		<div class="content round boxshadow">
			<div class="lc">							
				<div class="sidenav round">
				<h2>Manage Account</h2>
				<ul>
					<li><a href="user.php">My Orders</a></li>
					<li><a href="changepass.php">Change Password</a></li>
				</ul>
				</div>
			</div>
<?php
	if(isset($_POST['submit']))	{
		$id=$_SESSION['c_id'];
		$un=$_SESSION['cname'];
		$op=$_POST['op'];
		$up=jp_encode($_POST['up']);
		
		$query="SELECT * FROM user WHERE username='$un' AND password='$op'";
		$query=$conn->query($query);
		if($query->num_rows==1){
		$query = "UPDATE `user` SET	password='$up' WHERE id='$id'"; 		
		$conn->query($query);
		$errors="Password Changed Successfully !";
		}
			else{
			$errors="Wrong old password";
			}
}	
		//redirect_to("page.php?pid=$pid");
	
?>
			<div class="rc round">
			<h2>Dashboard</h2>
		<form class="form_area" action="changepass.php" method="POST" >
		<table cellpadding="0" cellspacing="0" width="730px" class="form_table">
							<tr>
						<td colspan="2">
							<?php
								if (!empty($errors)) {
									echo "<span style=\"color:red; display:block; background-color:yellow; text-align:center\"> ** {$errors} ** </span>";
								}
							?>
						   

						</td>
					</tr>
		<tr>
			<td width="96">Username</td>
			<td width="632"><input type="text" name="un" disabled="disabled" value="<?=$_SESSION['cname'];?>" /></td>
		</tr>
		<tr>
			<td>Old Password</td>
			<td><input type="password" name="op"/></td>
		</tr>
		<tr>
			<td>New Password</td>
			<td><input type="password" name="up"/></td>
		</tr>

		<tr>
		<td>&nbsp;</td>
		<td>
		<input class="button" name="submit" value="Change Password" type="submit"/>
		<input class="button" type="reset" />
		</td>
		</tr>

		</table>
		</form>
			
			</div>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>