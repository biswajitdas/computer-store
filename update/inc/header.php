<?php require_once("session.php"); ?>
<?php require_once("connect.php"); ?>
<?php require_once("functions.php"); ?>
<?php confirm_logged_in(); ?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Computers Store Admin Panel</title>
	<link rel="stylesheet" type="text/css" href="data/style.css" />
	<link rel="stylesheet" href="redactor/css/redactor.css" />
    <script type="text/javascript" src="data/jquery-1.7.min.js"></script>	

	<script src="redactor/redactor.js"></script>
	
	<script type="text/javascript"> 
	$(document).ready(
		function()
		{
			$('#redactor').redactor({ focus: true });
		}
	);
	
	$(document).ready(function(){		
		$("#msg").animate({opacity:'hide'},5000);
	});
		
	</script>
		
</head>
<body>
	<div class="wrapper round">
		<div class="header roundbottom">
		<h2>Computers Store</h2>
			<div class="topnav">
			<ul>
				<li><a href="admin.php">Dashboard</a></li>
				<li><a href="http://biswajitdas.me/projects/cstore" target="_blank">Back to website</a></li>
				<li><a href="logout.php">Log Out</a></li>
			</ul>
			</div>
		</div>
		<div class="content round boxshadow">
			<div class="lc">
				<div class="sidenav round">
				<h2>Manage Products</h2>
				<ul>
					<li><a href="cat.php">Catagories</a></li>
					<li><a href="addcat.php">Add A Catagory</a></li>
					<li><a href="subcat.php">Sub Catagories</a></li>
					<li><a href="addsubcat.php">Add A Sub Catagory</a></li>
					<li><a href="products.php">Show || Edit Products</a></li>
					<li><a href="addproducts.php">Add A Product</a></li>
				</ul>
				</div>
				
				<!--div class="sidenav round">
				<h2>Manage Discounts</h2>
				<ul>
					<li><a href="catdiscount.php">By Category</a></li>
					<li><a href="scdiscount.php">By Sub Catagory</a></li>
				</ul>
				</div-->
				
				<div class="sidenav round">
				<h2>Manage Orders</h2>
				<ul>
					<li><a href="approval.php">Order Aproval</a></li>
					<li><a href="vendor.php">Order to Vendor</a></li>
				</ul>
				</div>
				
				<div class="sidenav round">
				<h2>Account</h2>
				<ul>
					<li><a href="cp.php">Change Password</a></li>
				</ul>
				</div>
			</div>