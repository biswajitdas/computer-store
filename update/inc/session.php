<?php
	session_start();
	
	function logged_in() {
		return isset($_SESSION['username']);
	}
	
	function confirm_logged_in() {
		if (!logged_in()) {
			redirect_to("index.php");
		}
	}
	
	function logged_in_customer() {
		return isset($_SESSION['cname']);
	}
	
	function confirm_logged_in_customer() {
		if (!logged_in_customer()) {
			redirect_to("update/signin.php");
		}
	}
	
?>
