<?php require_once("update/inc/session.php");?>
<?php require_once("update/inc/connect.php"); ?>
<?php require_once("update/inc/functions.php"); ?>
<!doctype html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<title>Computer Store</title>
<link rel="stylesheet" type="text/css" href="data/reset.css"/>
<link rel="stylesheet" type="text/css" href="data/style.css"/>
<script type="text/javascript" src="data/jquery-1.12.3.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	
	//$(".rc h2:first").addClass("active");
	//$(".rc ul:not(:first)").hide();
	$(".rc ul").hide();

	$(".rc h2").click(function(){
		$(this).next("ul").slideToggle("slow").siblings("ul:visible").slideUp("slow");
		$(this).toggleClass("active");
		$(this).siblings("h2").removeClass("active");
	});

});
</script>
</head>
<body>
<div class="wrapper">
<div class="header">
<div class="nav">
<ul>
<li><a href="index.php">Home</a></li>
<li><a href="products.php?page=1&ipp=5">Products</a></li>
<li><a href="contact.php">Contact Us</a></li>
<li><a href="update/signin.php">Sign In</a></li>
<li><a href="signup.php">Sign Up</a></li>
<li><a href="mycart.php">My Cart</a></li>
</ul>
</div>	
</div> <!-- End of Header -->